# Geolocation Exchange API #

### What is this repository for? ###

- REST API for Geo location
- interceptor that checks that Bearer token was passed (compare with value specified in properties)
- @RestControllerAdvice to handle exceptions
- store Geo locations in MongoDB
- RabbitMQ for new Geo locations processing
- implementation of error handling within processing the Geo location
- enrichment includes updating GeoData with geocoding date using FeignClient that calls some open API

Note: implementation according to [Hexagonal Architecture](https://netflixtechblog.com/ready-for-changes-with-hexagonal-architecture-b315ec967749).
[Hexagonal Architecture](https://www.baeldung.com/hexagonal-architecture-ddd-spring)

Flow: Controller -> Service/Producer -> Geo locations Exchange -> Geo locations Queue -> Listener -> Enrichment -> MongoDB

#### Notes
What is GeoJSON in MongoDB?

GeoJSON is a geospatial data interchange format based
on JavaScript Object Notation (JSON).
GeoJSON uses a geographic coordinate reference system,
World Geodetic System 1984, and units of decimal degrees.
It is a format widely used across JSON-based applications to read, manipulate and compare geospatial data.

### How do I get set up? ###
_Requirements_

Java 11

Gradle

Spring Boot (Web, Data, AMQP and Security) + Spring Cloud OpenFeign

Bearer Token

MongoDB

RabbitMQ

Testing (JUnit 5, Mockito, WireMock, TestRestTemplate)
### Mongo Docker
[mongo(official guide)](https://hub.docker.com/_/mongo)
##### MONGO_INITDB_DATABASE
This variable allows you to specify
the name of a database to be used for creation scripts
in /docker-entrypoint-initdb.d/*.js.
MongoDB is fundamentally designed for "create on first use", so if you do not insert data with your JavaScript files,
then **no database is created**.

Note: The code in the docker-entrypoint-init.d folder is only executed if the
database has never been initialized before.

### Prerequisites 
````shell
$ cd geo-exchange-api
$ docker-compose up --build

$ docker-compose exec mongodb /bin/sh

# Stop running containers
$ docker-compose down --remove-orphans --volumes

````

### Mongodb-express
mongo express is available on http://localhost:8081/

### RabbitMQ
[Dockerizing a RabbitMQ Instance using Docker Containers](https://www.section.io/engineering-education/dockerize-a-rabbitmq-instance/)
If you open http://localhost:15672/ on a browser, 
you will be able to access the management Ui, 
and now you can log in using the docker-compose set username and password.

###### Register the Listener and Send a Message

Spring AMQP’s RabbitTemplate provides everything you need to send and receive messages with RabbitMQ. 
However, you need to:

    Declare the **queue**, the **exchange**, and the binding between them.

Queue: geo-queue

Exchange: geo-exchange

### Build and Run
```
cd geo-exchange-api
$ ./gradlew build && ./gradlew :application:bootRun
```
#### URL
[http://localhost:9091](http://localhost:9091)

#### Dev notes
[Insert Data Into MongoDB Container at the Spring Boot Application initialization](https://stackoverflow.com/questions/67715710/insert-data-into-mongodb-container-at-the-spring-boot-application-initialization)
[Accessing Data with MongoDB](https://spring.io/guides/gs/accessing-data-mongodb/)

To get coordinates by provided Location:

[Nominatim API](https://nominatim.org/release-docs/develop/api/Overview/)

To Setup GEO RabbitMQ
[Messaging with RabbitMQ](https://spring.io/guides/gs/messaging-rabbitmq/)

#### How to fix
* Document{{type=Point, coordinates=[0.0, 0.0]}} into type class com.mongodb.client.model.geojson.Point but didn't find a PersistentEntity for the latter!
[GeoJson serialization issues with Spring Data MongoDb](https://www.alessandrorosa.com/geojson-serialization-issues-with-spring-data-mongodb/)
  
* .DomainException: Cannot construct instance of `com.mongodb.client.model.geojson.Point` 
  (no Creators, like default constructor, exist): 
  cannot deserialize from Object value (no delegate- or property-based Creator)
  at [Source: (String)"{"id":"4","location":"Albania",
    "point":{"coordinateReferenceSystem":null,"type":"POINT","position":{"values":[0.0,0.0]},
        "coordinates":{"values":[0.0,0.0]}}}"; 
  line: 1, column: 41] (through reference chain: com.tst.geoexchangeapi.domain.GeoData["point"])
  



