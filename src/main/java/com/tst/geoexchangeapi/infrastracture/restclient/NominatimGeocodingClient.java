package com.tst.geoexchangeapi.infrastracture.restclient;

import com.tst.geoexchangeapi.domain.restclient.GeocodingClient;

public class NominatimGeocodingClient implements GeocodingClient {

    private final SpringGeocodingClient geocodingClient;

    public NominatimGeocodingClient(SpringGeocodingClient geocodingClient) {
        this.geocodingClient = geocodingClient;
    }

    @Override
    public GeocodingResponse search(SearchParams params) {
        return geocodingClient.search(params);
    }
}
