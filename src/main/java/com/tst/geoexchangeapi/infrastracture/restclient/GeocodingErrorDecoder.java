package com.tst.geoexchangeapi.infrastracture.restclient;

import com.tst.geoexchangeapi.domain.exception.BadRequestException;
import com.tst.geoexchangeapi.domain.exception.NotFoundException;

import feign.Response;
import feign.codec.ErrorDecoder;

public class GeocodingErrorDecoder implements ErrorDecoder {
    @Override
    public Exception decode(String methodKey, Response response) {
        switch (response.status()) {
            case 400:
                return new BadRequestException("Failed parse request. MethodKey: " + methodKey);
            case 404:
                return new NotFoundException("Not found. MethodKey: " + methodKey);
            default:
                return new Exception("Generic error");
        }
    }
}
