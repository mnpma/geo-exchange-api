package com.tst.geoexchangeapi.infrastracture.restclient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "nominatim", url = "https://nominatim.openstreetmap.org")
public interface SpringGeocodingClient {

    @RequestMapping(method = RequestMethod.GET, value = "/search")
    GeocodingResponse search(@SpringQueryMap SearchParams params);
}
