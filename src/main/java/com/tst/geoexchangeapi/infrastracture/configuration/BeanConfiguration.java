package com.tst.geoexchangeapi.infrastracture.configuration;

import com.tst.geoexchangeapi.GeoExchangeApiApplication;
import com.tst.geoexchangeapi.domain.repository.GeoRepository;
import com.tst.geoexchangeapi.domain.restclient.GeocodingClient;
import com.tst.geoexchangeapi.domain.service.*;
import com.tst.geoexchangeapi.infrastracture.queue.MessageProducer;
import com.tst.geoexchangeapi.infrastracture.queue.RabbitMQMessageProducer;
import com.tst.geoexchangeapi.infrastracture.queue.Receiver;
import com.tst.geoexchangeapi.infrastracture.restclient.SpringGeocodingClient;
import com.tst.geoexchangeapi.infrastracture.restclient.GeocodingErrorDecoder;
import com.tst.geoexchangeapi.infrastracture.restclient.NominatimGeocodingClient;
import feign.codec.ErrorDecoder;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = GeoExchangeApiApplication.class)
public class BeanConfiguration {

    @Value("${token:123}")
    private String bearerAccessToken;

    @Value("${topic:geo-exchange}")
    private String topicExchangeName;

    @Value("${queue:geo-queue}")
    private String queueName;

    @Value("${routingKey:geo}")
    private String routingKey;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Bean
    public GeocodingClient geocodingClient(final SpringGeocodingClient geocodingClient) {
        return new NominatimGeocodingClient(geocodingClient);
    }

    @Bean
    public GeoService geoService(final GeoRepository geoRepository,
                                 final SpringGeocodingClient geocodingClient) {
        return new DomainGeoService(geoRepository, geocodingClient(geocodingClient));
    }

    @Bean
    public AuthService authService() {
        return new BearerTokenAuthService(bearerAccessToken);
    }

    @Bean
    public AuthHandlerInterceptor authHandlerInterceptor() {
        return new AuthHandlerInterceptor(authService());
    }

    @Bean
    public ErrorDecoder errorDecoder() {
        return new GeocodingErrorDecoder();
    }

    @Bean
    Queue queue() {
        return new Queue(queueName, false);
    }

    @Bean
    TopicExchange exchange() {
        return new TopicExchange(topicExchangeName);
    }

    @Bean
    Binding binding(Queue queue, TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(routingKey);
    }

    @Bean
    SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
                                             MessageListenerAdapter listenerAdapter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(queueName);
        container.setMessageListener(listenerAdapter);
        return container;
    }

    @Bean
    MessageListenerAdapter listenerAdapter(Receiver receiver) {
        return new MessageListenerAdapter(receiver, "receiveMessage");
    }

    @Bean
    public Receiver receiver(final GeoRepository geoRepository,
                             final SpringGeocodingClient geocodingClient) {
        return new Receiver(geoRepository, geocodingClient(geocodingClient));
    }

    @Bean
    public MessageProducer messageProducer(final GeoRepository geoRepository,
                                           final SpringGeocodingClient geocodingClient) {
        RabbitMQMessageProducer producer = new RabbitMQMessageProducer(rabbitTemplate,
                receiver(geoRepository, geocodingClient));
        producer.setRoutingKey(routingKey);
        producer.setTopicExchangeName(topicExchangeName);
        return producer;
    }

    @Bean
    public GeoExchangeService geoExchangeService(final GeoRepository geoRepository,
                                                 final SpringGeocodingClient geocodingClient) {
        return new DomainGeoExchangeService(messageProducer(geoRepository, geocodingClient));
    }
}
