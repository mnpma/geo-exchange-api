package com.tst.geoexchangeapi.infrastracture.configuration;

import com.tst.geoexchangeapi.domain.exception.DomainException;
import com.tst.geoexchangeapi.domain.exception.UnauthorizedException;
import com.tst.geoexchangeapi.domain.service.AuthService;
import lombok.extern.slf4j.Slf4j;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
public class AuthHandlerInterceptor implements HandlerInterceptor {

    private final AuthService authService;

    public AuthHandlerInterceptor(AuthService authService) {
        this.authService = authService;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("Check Bearer token is passed. Method: {}, URL: {}, ",
                request.getMethod(),
                request.getRequestURI());

        try {
            String authHeaderValue = request.getHeader("Authorization");
            if (!authService.validateToken(authHeaderValue)) {
                throw new UnauthorizedException("Not authorized.");
            }
            return true;
        } catch (UnauthorizedException e) {
            log.error(e.getMessage());
            throw e;
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new DomainException(e.getMessage());
        }
    }
}
