package com.tst.geoexchangeapi.infrastracture.configuration;

import com.tst.geoexchangeapi.infrastracture.repository.mongo.SpringDataMongoGeoRepository;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@EnableMongoRepositories(basePackageClasses = SpringDataMongoGeoRepository.class)
public class MongoDbConfiguration {
}
