package com.tst.geoexchangeapi.infrastracture.repository.mongo;

import com.tst.geoexchangeapi.domain.GeoData;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SpringDataMongoGeoRepository extends MongoRepository<GeoData, String> {
}
