package com.tst.geoexchangeapi.infrastracture.repository.mongo;

import com.tst.geoexchangeapi.domain.GeoData;
import com.tst.geoexchangeapi.domain.repository.GeoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Slf4j
@Component
@Primary
public class MongoDbGeoRepository implements GeoRepository {

    private final SpringDataMongoGeoRepository geoRepository;

    @Autowired
    public MongoDbGeoRepository(SpringDataMongoGeoRepository geoRepository) {
        this.geoRepository = geoRepository;
    }

    @Override
    public Optional<GeoData> findById(final String id) {
        return geoRepository.findById(id);
    }

    @Override
    public void save(final GeoData geoData) {
        GeoData savedGeoData = geoRepository.save(geoData);
        log.debug("id: {}, location: {}", savedGeoData.getId(), savedGeoData.getLocation());
    }
}
