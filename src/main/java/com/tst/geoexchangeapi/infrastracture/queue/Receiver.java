package com.tst.geoexchangeapi.infrastracture.queue;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.model.geojson.Point;
import com.mongodb.client.model.geojson.Position;
import com.tst.geoexchangeapi.domain.GeoData;
import com.tst.geoexchangeapi.domain.Geometry;
import com.tst.geoexchangeapi.domain.exception.DomainException;
import com.tst.geoexchangeapi.domain.repository.GeoRepository;
import com.tst.geoexchangeapi.domain.restclient.GeocodingClient;
import com.tst.geoexchangeapi.infrastracture.restclient.GeocodingResponse;
import com.tst.geoexchangeapi.infrastracture.restclient.SearchParams;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.concurrent.CountDownLatch;

@Slf4j
public class Receiver {

    private final ObjectMapper objectMapper = new ObjectMapper();

    private CountDownLatch latch = new CountDownLatch(1);

    private final GeoRepository geoRepository;

    private final GeocodingClient geocodingClient;

    public Receiver(GeoRepository geoRepository, GeocodingClient geocodingClient) {
        this.geoRepository = geoRepository;
        this.geocodingClient = geocodingClient;
    }

    public void receiveMessage(String message) {
        try {
            GeoData geoData = objectMapper.readValue(message, GeoData.class);

            final String countryName = geoData.getLocation().replace("&", "%26");
            SearchParams params = SearchParams.builder()
                    .q(countryName)
                    .format("geojson")
                    .build();

            GeocodingResponse response = geocodingClient.search(params);

            List<Double> coordinates = response.getFeatures().stream()
                    .filter(item -> "place".equalsIgnoreCase(item.getProperties().getCategory()))
                    .map(item -> item.getGeometry().getCoordinates())
                    .findAny()
                    .orElseGet(() -> List.of(0.0, 0.0));

            Geometry point = new Geometry("POINT", coordinates);
            geoData.setGeometry(point);
            geoRepository.save(geoData);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
            throw new DomainException(e.getMessage());
        } finally {
            latch.countDown();
        }
    }

    public CountDownLatch getLatch() {
        return latch;
    }
}
