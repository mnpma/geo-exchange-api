package com.tst.geoexchangeapi.infrastracture.queue;

import com.tst.geoexchangeapi.domain.exception.DomainException;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import java.util.concurrent.TimeUnit;

@Slf4j
@Setter
public class RabbitMQMessageProducer implements MessageProducer {

    private final RabbitTemplate rabbitTemplate;
    private final Receiver receiver;

    private String topicExchangeName;
    private String routingKey;

    public RabbitMQMessageProducer(RabbitTemplate rabbitTemplate, Receiver receiver) {
        this.rabbitTemplate = rabbitTemplate;
        this.receiver = receiver;
    }

    public void produce(String message) {
        rabbitTemplate.convertAndSend(topicExchangeName, routingKey, message);
        try {
            receiver.getLatch().await(10000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            log.error(e.getMessage());
            throw new DomainException(e.getMessage());
        }
    }
}
