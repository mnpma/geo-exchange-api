package com.tst.geoexchangeapi.infrastracture.queue;

public interface MessageProducer {
    void produce(String message);
}
