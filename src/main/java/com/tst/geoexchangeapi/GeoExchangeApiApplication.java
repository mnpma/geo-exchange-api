package com.tst.geoexchangeapi;

import com.tst.geoexchangeapi.application.CliGeoController;
import com.tst.geoexchangeapi.domain.GeoData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.PropertySource;

import java.util.Locale;

import java.util.stream.IntStream;

@Slf4j
@SpringBootApplication
@EnableFeignClients
@PropertySource(value = {"classpath:application.yml"})
public class GeoExchangeApiApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(GeoExchangeApiApplication.class);
        application.run(args);
    }

    @Autowired
    public CliGeoController cliGeoController;

    @Override
    public void run(String... args) throws Exception {
        String[] locales = Locale.getISOCountries();

        IntStream.range(0, 10)
                .forEach(id -> {
                    String countryCode = locales[id < locales.length ? id : locales.length - 1];
                    Locale obj = new Locale("en", countryCode);

                    log.debug("Geo data with random id {} is being generated...", id);
                    cliGeoController.createGeoData(new GeoData(String.valueOf(id), obj.getDisplayCountry(Locale.ENGLISH)));
                });
    }
}
