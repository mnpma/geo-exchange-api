package com.tst.geoexchangeapi.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "geoData")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GeoData {

    @Id
    private String id;
    private String location;
    //@GeoSpatialIndexed(type = GeoSpatialIndexType.GEO_2DSPHERE)
    //private Point point;
    private Geometry geometry;

    public GeoData(String id, String location) {
        this.id = id;
        this.location = location;
        this.geometry = new Geometry("POINT", List.of(0.0, 0.0));
    }
}
