package com.tst.geoexchangeapi.domain.repository;

import com.tst.geoexchangeapi.domain.GeoData;

import java.util.Optional;

public interface GeoRepository {
    Optional<GeoData> findById(String id);

    void save(GeoData geoData);
}
