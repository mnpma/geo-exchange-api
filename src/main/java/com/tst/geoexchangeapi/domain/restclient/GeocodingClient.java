package com.tst.geoexchangeapi.domain.restclient;

import com.tst.geoexchangeapi.infrastracture.restclient.GeocodingResponse;
import com.tst.geoexchangeapi.infrastracture.restclient.SearchParams;

public interface GeocodingClient {
    GeocodingResponse search(SearchParams params);
}
