package com.tst.geoexchangeapi.domain.service;

public interface GeoExchangeService {
    void exchangeGeoData(String id, String location);
}
