package com.tst.geoexchangeapi.domain.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tst.geoexchangeapi.domain.GeoData;
import com.tst.geoexchangeapi.domain.exception.DomainException;
import com.tst.geoexchangeapi.infrastracture.queue.MessageProducer;
import com.tst.geoexchangeapi.infrastracture.restclient.SearchParams;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DomainGeoExchangeService implements GeoExchangeService {

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final MessageProducer messageProducer;

    public DomainGeoExchangeService(MessageProducer messageProducer) {
        this.messageProducer = messageProducer;
    }

    @Override
    public void exchangeGeoData(String id, String location) {
        GeoData newGeoData = new GeoData(id, location);
        String message;
        try {
            message = objectMapper.writeValueAsString(newGeoData);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
            throw new DomainException(e.getMessage());
        }
        messageProducer.produce(message);
    }
}
