package com.tst.geoexchangeapi.domain.service;

import com.mongodb.client.model.geojson.Point;
import com.mongodb.client.model.geojson.Position;
import com.tst.geoexchangeapi.domain.GeoData;
import com.tst.geoexchangeapi.domain.Geometry;
import com.tst.geoexchangeapi.domain.exception.DomainException;
import com.tst.geoexchangeapi.domain.repository.GeoRepository;
import com.tst.geoexchangeapi.domain.restclient.GeocodingClient;
import com.tst.geoexchangeapi.infrastracture.restclient.GeocodingResponse;
import com.tst.geoexchangeapi.infrastracture.restclient.SearchParams;

import java.util.List;

public class DomainGeoService implements GeoService {

    private final GeoRepository geoRepository;

    private final GeocodingClient geocodingClient;

    public DomainGeoService(GeoRepository geoRepository, GeocodingClient geocodingClient) {
        this.geoRepository = geoRepository;
        this.geocodingClient = geocodingClient;
    }

    @Override
    public void createGeoData(GeoData geoData) {
        geoRepository.save(geoData);
    }

    @Override
    public void exchangeGeoData(String id, final String location) {
        final String countryName = location.replace("&", "%26");
        SearchParams params = SearchParams.builder()
                .q(countryName)
                .format("geojson")
                .build();
        GeocodingResponse response = geocodingClient.search(params);

        List<Double> coordinates = response.getFeatures().stream()
                .filter(item -> "place".equalsIgnoreCase(item.getProperties().getCategory()))
                .map(item -> item.getGeometry().getCoordinates())
                .findAny()
                .orElseGet(() -> List.of(0.0, 0.0));

        GeoData geoData = new GeoData(id, location, new Geometry("POINT", coordinates));
        geoRepository.save(geoData);
    }

    @Override
    public GeoData getGeoData(String id) {
        return geoRepository.findById(id).orElseThrow(
                () -> new DomainException("Failed to find geolocation by id: " + id)
        );
    }
}
