package com.tst.geoexchangeapi.domain.service;

import com.tst.geoexchangeapi.domain.GeoData;

public interface GeoService {
    void createGeoData(GeoData geoData);

    void exchangeGeoData(String id, String location);

    GeoData getGeoData(String id);
}
