package com.tst.geoexchangeapi.domain.service;

public interface AuthService {

    boolean validateToken(String tokenValue);
}
