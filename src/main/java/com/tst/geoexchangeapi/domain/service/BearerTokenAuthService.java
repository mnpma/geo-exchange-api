package com.tst.geoexchangeapi.domain.service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BearerTokenAuthService implements AuthService {

    private final String bearerAccessToken;

    public BearerTokenAuthService(String bearerAccessToken) {
        log.debug("Token: {}", bearerAccessToken);
        this.bearerAccessToken = bearerAccessToken;
    }

    @Override
    public boolean validateToken(String tokenValue) {
        return tokenValue != null && tokenValue.contains("Bearer ")
                && tokenValue.replace("Bearer ", "").equals(bearerAccessToken);
    }
}
