package com.tst.geoexchangeapi.application.rest;

import com.tst.geoexchangeapi.domain.GeoData;
import com.tst.geoexchangeapi.domain.service.GeoExchangeService;
import com.tst.geoexchangeapi.domain.service.GeoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/geo")
public class GeoController {

    private final GeoService geoService;

    private final GeoExchangeService geoExchangeService;

    @Autowired
    public GeoController(GeoService geoService, GeoExchangeService geoExchangeService) {
        this.geoService = geoService;
        this.geoExchangeService = geoExchangeService;
    }

    @GetMapping(value = "/{id}/{location}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void exchangeGeo(@PathVariable final String id, @PathVariable String location) {
        //geoService.exchangeGeoData(id, location);
        geoExchangeService.exchangeGeoData(id, location);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public GeoData getGeoData(@PathVariable final String id) {
        return geoService.getGeoData(id);
    }
}
