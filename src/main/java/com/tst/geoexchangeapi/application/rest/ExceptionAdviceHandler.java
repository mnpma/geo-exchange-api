package com.tst.geoexchangeapi.application.rest;

import com.tst.geoexchangeapi.domain.exception.BadRequestException;
import com.tst.geoexchangeapi.domain.exception.DomainException;
import com.tst.geoexchangeapi.domain.exception.NotFoundException;
import com.tst.geoexchangeapi.domain.exception.UnauthorizedException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@SuppressWarnings("rawtypes")
@Slf4j
@ControllerAdvice
public class ExceptionAdviceHandler {

    @ExceptionHandler(UnauthorizedException.class)
    ResponseEntity<String> handleUnauthorizedException(HttpServletRequest request, Throwable ex) {
        return new ResponseEntity(ex.getMessage(), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(NotFoundException.class)
    ResponseEntity<String> handleNotFoundException(HttpServletRequest request, Throwable ex) {
        return new ResponseEntity(ex.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(BadRequestException.class)
    ResponseEntity<String> handleBadRequestException(HttpServletRequest request, Throwable ex) {
        return new ResponseEntity(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DomainException.class)
    ResponseEntity<String> handleControllerException(HttpServletRequest request, Throwable ex) {
        log.error("Error to process request URI: {}. Caused by {}", request.getRequestURI(), ex.getMessage());
        return new ResponseEntity(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
