package com.tst.geoexchangeapi.application;

import com.tst.geoexchangeapi.domain.GeoData;
import com.tst.geoexchangeapi.domain.service.GeoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class CliGeoController {

    private final GeoService geoService;

    public CliGeoController(GeoService geoService) {
        this.geoService = geoService;
    }

    public void createGeoData(GeoData geoData) {
        geoService.createGeoData(geoData);
    }
}
