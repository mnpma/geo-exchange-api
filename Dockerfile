FROM adoptopenjdk/openjdk11:jdk-11.0.5_10-alpine
LABEL maintainer="mnfpma@gmail.com"
ADD build/libs/geo-exchange-api-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 9091
ENTRYPOINT ["java", "-jar","/app.jar"]