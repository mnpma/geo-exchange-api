var db = connect("mongodb://admin:14292@127.0.0.1:27017/admin");

db = db.getSiblingDB('geo_db'); /* 'use' statement doesn't support here to switch db */

db.createUser(
    {
        user: "geo",
        pwd: "14292",
        roles: [ { role: "readWrite", db: "geo_db" } ]
    }
);
db.createCollection("geoData");
// add new collection

db.geoData.insert({"id": "-1", "location": "Paris"});